public with sharing class GitLab implements Git {

    public String push(){
        return 'pushed to GitLab';
    }
    public String pull(){
        return 'pull from GitLab';
    }
    public String comnit(){
        return 'commit to GitLab';
    }
}
