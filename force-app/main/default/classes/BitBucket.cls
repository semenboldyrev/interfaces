public with sharing class BitBucket implements Git {

    public String push(){
        return 'pushed to BitBucket';
    }
    public String pull(){
        return 'pull from BitBucket';
    }
    public String comnit(){
        return 'commit to BitBucket';
    }
}
